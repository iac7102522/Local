#!/bin/bash

debian_arch=$(dpkg --print-architecture)
case "$debian_arch" in
    amd64)
        echo "amd64"
        ;;
    arm64)
        echo "arm64"
        ;;
    *)
        echo "Unsupported architecture: $debian_arch"
        exit 1
        ;;
esac
