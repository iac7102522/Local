#!/bin/bash

# Ensure the arch parameter
if [ -z "$1" ]; then
    echo "Usage: $0 <amd64|arm64>"
    exit 1
fi

# Define the correct URL
case "$1" in
    amd64)
        ssm_package_url="https://s3.amazonaws.com/session-manager-downloads/plugin/latest/ubuntu_64bit/session-manager-plugin.deb"
        ;;
    arm64)
        ssm_package_url="https://s3.amazonaws.com/session-manager-downloads/plugin/latest/ubuntu_arm64/session-manager-plugin.deb"
        ;;
    *)
        echo "Unsuported Architecture: $1"
        exit 1
        ;;
esac

# Download
curl -o /tmp/session-manager-plugin.deb "$ssm_package_url"

# Install
dpkg -i /tmp/session-manager-plugin.deb

# Remove unecessary files
rm /tmp/session-manager-plugin.deb
