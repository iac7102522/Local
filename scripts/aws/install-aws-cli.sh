#!/bin/bash

# Ensure the arch parameter
if [ -z "$1" ]; then
    echo "Usage: $0 <amd64|arm64>"
    exit 1
fi

# Define the correct URL
case "$1" in 
    amd64) 
        aws_cli_package_url="https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" 
        ;;
    arm64) \
        aws_cli_package_url="https://awscli.amazonaws.com/awscli-exe-linux-aarch64.zip" 
        ;;
    *)
        echo "Architecture not supported: $1" 
        exit 1
        ;;
esac

# Download
curl -o /tmp/aws-cli.zip "$aws_cli_package_url"
unzip /tmp/aws-cli.zip -d /tmp/
chmod +x /tmp/aws/install

# Install
/tmp/aws/install

# Remove unecessary files
rm /tmp/aws-cli.zip
rm -rf /tmp/aws
