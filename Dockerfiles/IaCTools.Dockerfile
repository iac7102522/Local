FROM debian:bookworm

# Dependencies
COPY ./scripts/detect-arch.sh /scripts/detect-arch.sh
COPY ./scripts/aws/install-aws-cli.sh /scripts/install-aws-cli.sh
COPY ./scripts/hashicorp/install-terraform.sh /scripts/install-terraform.sh

RUN chmod +x /scripts/*.sh

RUN apt update && apt install -y \
    gnupg \
    openssh-client \
    iputils-ping \
    dnsutils \
    curl \
    coreutils \
    unzip \
    software-properties-common \
    wget


# Get arch and install dependencies
RUN arch=$(/scripts/detect-arch.sh) && \
    /scripts/install-aws-cli.sh "$arch" && \
    /scripts/install-terraform.sh

WORKDIR /projects

# Keeps the container running
CMD ["/bin/bash", "-c", "while true; do sleep 1000; done"]
